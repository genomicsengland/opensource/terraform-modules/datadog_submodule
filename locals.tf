locals {
  task_actions = [
    "cloudwatch:*",
    "logs:CreateLogGroup",
    "logs:CreateLogStream",
    "logs:PutLogEvents",
    "logs:DescribeLogStreams",
  ]
  execution_actions = [
    "secretsmanager:GetSecretValue",
    "ssm:GetParameters",
    "kms:Decrypt",
    "kms:ListKeys",
    "kms:ListAliases",
    "kms:DescribeKey"
  ]
  merged = [for container_definition in var.container_definitions : merge(container_definition, local.log_configuration)]
}

locals {
  log_configuration = {
    logConfiguration = {
      logDriver = "awsfirelens"
      options = {
        Name       = "datadog"
        dd_service = var.application
        Host       = "http-intake.logs.datadoghq.eu"
        dd_source  = "httpd"
        dd_tags    = var.application
        TLS        = "on"
        provider   = "ecs"
      },
      secretOptions = [
        {
          name      = "apikey"
          valueFrom = var.datadog_api_key
        },
      ]
    }
  }
}

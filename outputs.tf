output "container_definitions" {
  value = concat([local.datadog_definition, local.log_router_definition], local.merged)
}

output "execution_actions" {
  value = local.execution_actions
}

output "task_actions" {
  value = local.task_actions
}
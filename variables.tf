variable "name_prefix" {
  type = string
}

variable "region" {
  type    = string
  default = "eu-west-2"
}

variable "log_router_image" {
  type    = string
  default = "877059142592.dkr.ecr.eu-west-2.amazonaws.com/fluentbit_basic:latest"
}

variable "datadog_image" {
  type    = string
  default = "877059142592.dkr.ecr.eu-west-2.amazonaws.com/ce_org_datadog_images:latest"
}

variable "datadog_logs_group" {
  type = string
}

variable "datadog_cpu" {
  type    = number
  default = 10
}

variable "datadog_memoryReservation" {
  type    = number
  default = 256
}

variable "datadog_api_key" {
  type        = string
  description = "DataDog API Key"
  default     = "arn:aws:secretsmanager:eu-west-2:512426816668:secret:/prod/core_shared/common/api_key/Datadog"
}

variable "application" {
  type = string
}

variable "datadog_tags" {
  type    = object({})
  default = {}
}

variable "container_definitions" {
  default = []
}